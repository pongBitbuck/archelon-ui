import { Injectable } from '@angular/core';
import { HttpHeaders,  HttpClient } from '@angular/common/http';


@Injectable()
export class GrapCompareService {

  constructor(private _http: HttpClient) { }

  async getDataGraphCompare(dataSearch){
    var headers = new HttpHeaders();
    console.log('dataSearch',dataSearch);
    headers.append("Content-type","application/json; charset=utf-8");
    let apiUrl = "http://149.28.154.112:4568/graph/getDataGraphCompare";
    //let apiUrl = "http://localhost:4568/graph/getDataGraphCompare";
    let res = await this._http.post(apiUrl,dataSearch, { headers :headers})
                        .toPromise()
                        .then()
                        .catch();
    console.log('res',res);
    return res;
  }

}
