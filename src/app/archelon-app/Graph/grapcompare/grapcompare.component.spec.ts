import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GrapCompareComponent } from './grapcompare.component';

describe('GrapCompareComponent', () => {
  let component: GrapCompareComponent;
  let fixture: ComponentFixture<GrapCompareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrapCompareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrapCompareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
