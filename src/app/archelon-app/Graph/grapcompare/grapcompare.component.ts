import { Component, NgModule, OnInit } from '@angular/core';
import { AmChartsService } from '@amcharts/amcharts3-angular';
import { GrapCompareService } from './GrapCompare.service';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

const coins = [
  {value: 'BCH', viewValue: 'BCH'}
  ,{value: 'BSV', viewValue: 'BSV'}
  ,{value: 'BTC', viewValue: 'BTC'}
  ,{value: 'EOS', viewValue: 'EOS'}
  ,{value: 'ETH', viewValue: 'ETH'}
  ,{value: 'EVX', viewValue: 'EVX'}
  ,{value: 'LTC', viewValue: 'LTC'}
  ,{value: 'OMG', viewValue: 'OMG'}
  ,{value: 'POW', viewValue: 'POW'}
  ,{value: 'REP', viewValue: 'REP'}
  ,{value: 'THB', viewValue: 'THB'}
  ,{value: 'USDT', viewValue: 'USDT'}
  ,{value: 'XRP', viewValue: 'XRP'}
  ,{value: 'XZC', viewValue: 'XZC'}
  ,{value: 'ZEC', viewValue: 'ZEC'}
  ,{value: 'DOG', viewValue: 'DOGE'}
];

const mainrateforms = [
  {value: 'THB', viewValue: 'THB'},
  {value: 'USDT', viewValue: 'USDT'},
  {value: 'USD', viewValue: 'USD'},
  {value: 'BTC', viewValue: 'BTC'}
];

const markets = [];

const marketsMain = [
  {value: 'BTCDOG', viewValue: 'BTC/DOGE', Market: 'Bx', BidFilter: 'BTC', AskFilter: 'DOG'},
  {value: 'BTCZEC', viewValue: 'BTC/ZEC', Market: 'Bx', BidFilter: 'BTC', AskFilter: 'ZEC'},
  {value: 'THBBCH', viewValue: 'THB/BCH', Market: 'Bx', BidFilter: 'THB', AskFilter: 'BCH'},
  {value: 'THBBSV', viewValue: 'THB/BSV', Market: 'Bx', BidFilter: 'THB', AskFilter: 'BSV'},
  {value: 'THBBTC', viewValue: 'THB/BTC', Market: 'Bx', BidFilter: 'THB', AskFilter: 'BTC'},
  {value: 'THBDAS', viewValue: 'THB/DASH', Market: 'Bx', BidFilter: 'THB', AskFilter: 'DASH'},
  {value: 'THBLTC', viewValue: 'THB/LTC', Market: 'Bx', BidFilter: 'THB', AskFilter: 'LTC'},
  {value: 'THBXZC', viewValue: 'THB/XZC', Market: 'Bx', BidFilter: 'THB', AskFilter: 'XZC'},
  {value: 'THBEOS', viewValue: 'THB/EOS', Market: 'Bx', BidFilter: 'THB', AskFilter: 'EOS'},
  {value: 'THBREP', viewValue: 'THB/REP', Market: 'Bx', BidFilter: 'THB', AskFilter: 'REP'},
  {value: 'THBXRP', viewValue: 'THB/XRP', Market: 'Bx', BidFilter: 'THB', AskFilter: 'XRP'},
  {value: 'THBOMG', viewValue: 'THB/OMG', Market: 'Bx', BidFilter: 'THB', AskFilter: 'OMG'},
  {value: 'THBPOW', viewValue: 'THB/POW', Market: 'Bx', BidFilter: 'THB', AskFilter: 'POW'},
  {value: 'THBETH', viewValue: 'THB/ETH', Market: 'Bx', BidFilter: 'THB', AskFilter: 'ETH'},
  {value: 'THBEVX', viewValue: 'THB/EVX', Market: 'Bx', BidFilter: 'THB', AskFilter: 'EVX'},
  {value: 'BCHABCBTC', viewValue: 'BCHABC/BTC', Market: 'Binance', BidFilter: 'BCH', AskFilter: 'BTC'},
  {value: 'BCHABCUSDT', viewValue: 'BCHABC/USDT', Market: 'Binance', BidFilter: 'BCH', AskFilter: 'USDT'},
  {value: 'BTCUSDT', viewValue: 'BTC/USDT', Market: 'Binance', BidFilter: 'BTC', AskFilter: 'USDT'},
  {value: 'DASHBTC', viewValue: 'DASH/BTC', Market: 'Binance', BidFilter: 'DASH', AskFilter: 'BTC'},
  {value: 'DASHUSDT', viewValue: 'DASH/USDT', Market: 'Binance', BidFilter: 'DASH', AskFilter: 'USDT'},
  {value: 'DOGEBTC', viewValue: 'DOGE/BTC', Market: 'Binance', BidFilter: 'DOG', AskFilter: 'BTC'},
  {value: 'DOGEUSDT', viewValue: 'DOGE/USDT', Market: 'Binance', BidFilter: 'DOG', AskFilter: 'USDT'},
  {value: 'EOSBTC', viewValue: 'EOS/BTC', Market: 'Binance', BidFilter: 'EOS', AskFilter: 'BTC'},
  {value: 'EOSUSDT', viewValue: 'EOS/USDT', Market: 'Binance', BidFilter: 'EOS', AskFilter: 'USDT'},
  {value: 'ETHBTC', viewValue: 'ETH/BTC', Market: 'Binance', BidFilter: 'ETH', AskFilter: 'BTC'},
  {value: 'ETHUSDT', viewValue: 'ETH/USDT', Market: 'Binance', BidFilter: 'ETH', AskFilter: 'USDT'},
  {value: 'EVXBTC', viewValue: 'EVX/BTC', Market: 'Binance', BidFilter: 'EVX', AskFilter: 'BTC'},
  {value: 'LTCBTC', viewValue: 'LTC/BTC', Market: 'Binance', BidFilter: 'LTC', AskFilter: 'BTC'},
  {value: 'LTCUSDT', viewValue: 'LTC/USDT', Market: 'Binance', BidFilter: 'LTC', AskFilter: 'USDT'},
  {value: 'OMGBTC', viewValue: 'OMG/BTC', Market: 'Binance', BidFilter: 'OMG', AskFilter: 'BTC'},
  {value: 'OMGUSDT', viewValue: 'OMG/USDT', Market: 'Binance', BidFilter: 'OMG', AskFilter: 'USDT'},
  {value: 'POWRBTC', viewValue: 'POWR/BTC', Market: 'Binance', BidFilter: 'POW', AskFilter: 'BTC'},
  {value: 'REPBTC', viewValue: 'REP/BTC', Market: 'Binance', BidFilter: 'REP', AskFilter: 'BTC'},
  {value: 'XRPBTC', viewValue: 'XRP/BTC', Market: 'Binance', BidFilter: 'XRP', AskFilter: 'BTC'},
  {value: 'XRPUSDT', viewValue: 'XRP/USDT', Market: 'Binance', BidFilter: 'XRP', AskFilter: 'USDT'},
  {value: 'XZCBTC', viewValue: 'XZC/BTC', Market: 'Binance', BidFilter: 'XZC', AskFilter: 'BTC'},
  {value: 'ZECBTC', viewValue: 'ZEC/BTC', Market: 'Binance', BidFilter: 'ZEC', AskFilter: 'BTC'},
  {value: 'ZECUSDT', viewValue: 'ZEC/USDT', Market: 'Binance', BidFilter: 'ZEC', AskFilter: 'USDT'}
];

const exchangers = [
  {name: 'Bx'}
  ,{name: 'Binance'}
  // ,{name: 'BITTREX'}
  // ,{name: 'POLONIEX'}
  // ,{name: 'HUOBI'}
  // ,{name: 'OKEX'}
];

@Component({
  selector: 'app-grapcompare',
  templateUrl: './grapcompare.component.html',
  styleUrls: ['./grapcompare.component.scss'],
  providers: [GrapCompareService]
})

@NgModule({
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB'}
  ],
})
export class GrapCompareComponent implements OnInit {
  private chart: any;
  coins = coins;
  mainrateforms = mainrateforms;
  markets = markets;
  marketsMain = marketsMain;
  exchangers = exchangers;
  coinsSelect = '';
  coinsSelect_vValue = '';
  rateSelect = '';
  rateSelect_vValue = '';
  fromDate: any;
  toDate: any;
  maxDate: Date = new Date();

  checked: boolean = false;
  indeterminate: boolean = false;
  align: string = 'start';
  disabled:boolean = false;

  MarketSelectValue = [
    { Market:'Bx', Value:'', text:'', bid:false, ask:false }
    ,{ Market:'Binance', Value:'', text:'', bid:false, ask:false }
  ];

  constructor( private _grapCompareService : GrapCompareService, private AmCharts: AmChartsService ) {
  }

  ngOnInit() {
  }

  drawGrap(){
    this.AmCharts.clear();
    setTimeout(()=>{
      this.drawGrapData();
    }, 3000);
  }

  coinsChange(e){
    this.coinsSelect = e.value;
    this.coinsSelect_vValue = this.coins.find(x => x.value == e.value).viewValue;
    this.setDefaultDataMarket();
    
  }

  rateChange(e){
    this.rateSelect = e.value;
    this.rateSelect_vValue = this.mainrateforms.find(x => x.value == e.value).viewValue;
  }
  
  marketChange(exchangerName,e){
    this.MarketSelectValue.find(x => x.Market == exchangerName).Value = e.value;
    let selectData = this.marketsMain.find(x => x.Market == exchangerName && x.value == e.value);
    this.MarketSelectValue.find(x => x.Market == exchangerName).text = selectData.viewValue;
  }

  exchangerChangeBidOrAsk(exchangerName,type,e){
    if(type === 'bid'){
      this.MarketSelectValue.find(x => x.Market == exchangerName).bid = e.checked;
    }
    else{
      this.MarketSelectValue.find(x => x.Market == exchangerName).ask = e.checked;
    }
  }

  getExchangerBidOrAskSelectValue(exchangerName,type){
    let res = false;
    let selectValue = this.MarketSelectValue.find(x => x.Market == exchangerName);

    if(type === 'bid'){
      res = selectValue.bid;
    }
    else{
      res = selectValue.ask;
    }

    return res;
  }

  getMarketSelectValue(exchangerName){
    let res = '';
    res = this.MarketSelectValue.find(x => x.Market == exchangerName).Value;

    return res;
  }

  setDefaultDataMarket(){
    this.MarketSelectValue.forEach(eachObj => {
      eachObj.Value = "";
      eachObj.bid = false;
      eachObj.ask = false;
    });
  }

  async drawGrapData(){
    let dataGrap;
    let dataSearch = {
      coinsSelect: this.coinsSelect,
      rateSelect: this.rateSelect,
      marketSelectValue: this.MarketSelectValue,
      dateFrom: this.fromDate,
      dateTo: this.toDate
    };
    let resData = await this._grapCompareService.getDataGraphCompare(dataSearch);
    dataGrap = resData;

    if((this.rateSelect != null && this.rateSelect.length > 0)
    && (this.rateSelect_vValue != null && this.rateSelect_vValue.length > 0)
    && (this.coinsSelect != null && this.coinsSelect.length > 0)
    && (this.coinsSelect_vValue != null && this.coinsSelect_vValue.length > 0)
    ){
      this.setValueGrap(dataGrap);
    }
  }

  setValueGrap(dataGrap){
    this.chart = this.AmCharts.makeChart('grapCompareChart', {
      'type': 'serial',
      'theme': 'light',
      'dataDateFormat': 'YYYY-MM-DD HH:NN',
      'dataProvider': dataGrap,
      "mouseWheelScrollEnabled": true,
      'addClassNames': true,
      'startDuration': 1,
      'marginLeft': 0,
      'hideBulletsCount': 50,
      'categoryField': 'date',
      'categoryAxis': {
        'autoGridCount': false,
        'gridCount': 50,
        'gridAlpha': 0.1,
        'gridColor': '#FFFFFF',
        'axisColor': '#555555',
        'dateFormats': [ 
          {
            'period': 'DD',
            'format': 'DD-MM-YYYY HH:NN'
          },
          {
            'period': 'WW',
            'format': 'MMM DD'
          },
          {
            'period': 'MM',
            'format': 'MMM'
          },
          {
            'period': 'YYYY',
            'format': 'YYYY'
          }
        ]
      },
      'valueAxes': [ {
        'id': 'a1',
        'title': this.rateSelect_vValue,
        'gridAlpha': 0,
        'axisAlpha': 0
      } ],
      'graphs': [ {
        'id': 'g1',
        'valueField': 'BxBid',
        'classNameField': 'bulletClass',
        'title': this.MarketSelectValue[0].Market,
        'type': 'line',
        'lineColor': '#0011ff',
        'lineThickness': 1,
        'behindColumns': false,
        'descriptionField': 'g1Name',
        'bullet': 'none',
        'bulletSize': 0,
        'bulletSizeField': 'townSize',
        'bulletBorderColor': '#0011ff',
        'bulletBorderAlpha': 1,
        'bulletBorderThickness': 0,
        'bulletColor': '#0011ff',
        'labelText': '[[g1Name2]]',
        'labelPosition': 'right',
        'showBalloon': true,
        'animationPlayed': true,
        'valueAxis': 'a1',
        'balloonText': 'Bid: [[value]]',
        'legendValueText': 'Bid: [[value]]',
        'legendPeriodValueText': 'Bid: [[value.sum]]',
      }, {
        'id': 'g2',
        'valueField': 'BxAsk',
        'classNameField': 'bulletClass',
        'title': this.MarketSelectValue[0].Market,
        'type': 'line',
        'lineColor': '#ff0000',
        'lineThickness': 1,
        'behindColumns': false,
        'descriptionField': 'g2Name',
        'bullet': 'none',
        'bulletSize': 0,
        'bulletSizeField': 'townSize',
        'bulletBorderColor': '#ff0000',
        'bulletBorderAlpha': 1,
        'bulletBorderThickness': 0,
        'bulletColor': '#ff0000',
        'labelText': '[[g2Name2]]',
        'labelPosition': 'right',
        'showBalloon': true,
        'animationPlayed': true,
        'valueAxis': 'a1',
        'balloonText': 'Ask: [[value]]',
        'legendValueText': 'Ask: [[value]]',
        'legendPeriodValueText': 'Ask: [[value.sum]]',
      }, {
        'id': 'g3',
        'valueField': 'BinaceBid',
        'classNameField': 'bulletClass',
        'title': this.MarketSelectValue[1].Market,
        'type': 'line',
        'lineColor': '#00d9ff',
        'lineThickness': 1,
        'behindColumns': false,
        'descriptionField': 'g3Name',
        'bullet': 'none',
        'bulletSize': 0,
        'bulletSizeField': 'townSize',
        'bulletBorderColor': '#00d9ff',
        'bulletBorderAlpha': 1,
        'bulletBorderThickness': 0,
        'bulletColor': '#00d9ff',
        'labelText': '[[g3Name2]]',
        'labelPosition': 'right',
        'showBalloon': true,
        'animationPlayed': true,
        'valueAxis': 'a1',
        'balloonText': 'Bid: [[value]]',
        'legendValueText': 'Bid: [[value]]',
        'legendPeriodValueText': 'Bid: [[value.sum]]',
      }, {
        'id': 'g4',
        'valueField': 'BinaceAsk',
        'classNameField': 'bulletClass',
        'title': this.MarketSelectValue[1].Market,
        'type': 'line',
        'lineColor': '#ff3c00',
        'lineThickness': 1,
        'behindColumns': false,
        'descriptionField': 'g4Name',
        'bullet': 'none',
        'bulletSize': 0,
        'bulletSizeField': 'townSize',
        'bulletBorderColor': '#ff3c00',
        'bulletBorderAlpha': 1,
        'bulletBorderThickness': 0,
        'bulletColor': '#ff3c00',
        'labelText': '[[g4Name2]]',
        'labelPosition': 'right',
        'showBalloon': true,
        'animationPlayed': true,
        'valueAxis': 'a1',
        'balloonText': 'Ask: [[value]]',
        'legendValueText': 'Ask: [[value]]',
        'legendPeriodValueText': 'Ask: [[value.sum]]',
      } ],
      'pathToImages': 'http://cdn.amcharts.com/lib/3/images/',
      'chartScrollbar': {
        'updateOnReleaseOnly': true
      },
      'chartCursor': {
        'zoomable': true,
        'categoryBalloonDateFormat': 'DD',
        'cursorAlpha': 0,
        'valueBalloonsEnabled': true,
        'cursorPosition': 'mouse'
      },
      'legend': {
        'bulletType': 'round',
        'equalWidths': false,
        'valueWidth': 120,
        'useGraphSettings': true,
      }
    });

    this.chart.addListener("rendered", this.zoomChart);
    this.zoomChart(dataGrap);
  }

  zoomChart(dataGrap){
    this.chart.zoomToIndexes(dataGrap.length - 15, dataGrap.length - 1);
  }

  mouseEnterBlockScore(){
  }

  mouseEnterUnBlockScore(){
  }

  //Date Change
  FromDateChange(e){
    this.fromDate = e.value;
    console.log('FromDate',this.fromDate);
  }

  ToDateChange(e){
    this.toDate = e.value;
    console.log('ToDate',this.toDate);
  }
}


export interface Phase1Item{
  coin : string;
  value: number;
}