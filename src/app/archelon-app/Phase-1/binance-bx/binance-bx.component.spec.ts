import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BinanceBxComponent } from './binance-bx.component';

describe('BinanceBxComponent', () => {
  let component: BinanceBxComponent;
  let fixture: ComponentFixture<BinanceBxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BinanceBxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BinanceBxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
