import { Phase1ServiceService } from '../phase1-service.service';
import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-binance-bx',
  templateUrl: './binance-bx.component.html',
  styleUrls: ['./binance-bx.component.scss'],
  providers: [Phase1ServiceService]
})
export class BinanceBxComponent implements OnInit {
  itemCoin : Array<Phase1Item> = new Array<Phase1Item>();

  constructor(private _phase1ServiceService : Phase1ServiceService ,  private router : Router) { }

  ngOnInit() {
    var that = this;
    

    

    this._phase1ServiceService.getInitialPhase1(1,2).subscribe(data => {
      data.result.forEach(element => {
        that.itemCoin.push({coin: element.shortName,value :element.amount , id: element.id});
      });
    });

    
    // this.itemCoin.push({coin:"BCHABC",value:0});
    // this.itemCoin.push({coin:"DASH",value:0});
    // this.itemCoin.push({coin:"DOGE",value:0});
    // this.itemCoin.push({coin:"EOS",value:0});
    // this.itemCoin.push({coin:"ETH",value:0});
    // this.itemCoin.push({coin:"EVX",value:0});
    // this.itemCoin.push({coin:"LTC",value:0});
    // this.itemCoin.push({coin:"OMG",value:0});
    // this.itemCoin.push({coin:"POWR",value:0});
    // this.itemCoin.push({coin:"REP",value:0});
    // this.itemCoin.push({coin:"XRP",value:0});
    // this.itemCoin.push({coin:"XZC",value:0});
    // this.itemCoin.push({coin:"ZEC",value:0});
  }

  onCancleClick() {
    this.router.navigate(["refresh-page/" + this.router.url.replace(/\//g, '^')]);
  }

  onSave() {
    console.log(this.itemCoin);
    this._phase1ServiceService.saveInfoPhase1(this.itemCoin).subscribe(data => {
        setTimeout(() => this.onCancleClick(), 2500);

    });

  }

}


export interface Phase1Item{
  coin : string;
  value: number;
  id: number;

}