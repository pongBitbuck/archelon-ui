import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BittrexBxComponent } from './bittrex-bx.component';

describe('BittrexBxComponent', () => {
  let component: BittrexBxComponent;
  let fixture: ComponentFixture<BittrexBxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BittrexBxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BittrexBxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
