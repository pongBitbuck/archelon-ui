import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BxBinanceComponent } from './bx-binance.component';

describe('BxBinanceComponent', () => {
  let component: BxBinanceComponent;
  let fixture: ComponentFixture<BxBinanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BxBinanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BxBinanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
