import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BxBittrexComponent } from './bx-bittrex.component';

describe('BxBittrexComponent', () => {
  let component: BxBittrexComponent;
  let fixture: ComponentFixture<BxBittrexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BxBittrexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BxBittrexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
