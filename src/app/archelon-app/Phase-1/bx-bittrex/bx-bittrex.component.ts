import { Component, OnInit } from '@angular/core';
import { Phase1ServiceService } from '../phase1-service.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-bx-bittrex',
  templateUrl: './bx-bittrex.component.html',
  styleUrls: ['./bx-bittrex.component.scss'],
  providers: [Phase1ServiceService]
})
export class BxBittrexComponent implements OnInit {
  itemCoin : Array<Phase1Item> = new Array<Phase1Item>();

  constructor(private _phase1ServiceService : Phase1ServiceService ,  private router : Router) { }

  ngOnInit() {
    var that = this;
    this._phase1ServiceService.getInitialPhase1(4,1).subscribe(data => {
      data.result.forEach(element => {
        that.itemCoin.push({coin: element.shortName,value :element.amount , id: element.id});
      });
    });
  }

  onCancleClick() {
    this.router.navigate(["refresh-page/" + this.router.url.replace(/\//g, '^')]);
  }

  onSave() {
    console.log(this.itemCoin);
    this._phase1ServiceService.saveInfoPhase1(this.itemCoin).subscribe(data => {
        setTimeout(() => this.onCancleClick(), 2500);

    });

  }

}


export interface Phase1Item{
  coin : string;
  value: number;
  id: number;

}