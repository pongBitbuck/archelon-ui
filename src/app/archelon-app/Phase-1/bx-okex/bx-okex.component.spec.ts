import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BxOkexComponent } from './bx-okex.component';

describe('BxOkexComponent', () => {
  let component: BxOkexComponent;
  let fixture: ComponentFixture<BxOkexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BxOkexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BxOkexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
