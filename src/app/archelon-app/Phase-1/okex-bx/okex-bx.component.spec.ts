import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OkexBxComponent } from './okex-bx.component';

describe('OkexBxComponent', () => {
  let component: OkexBxComponent;
  let fixture: ComponentFixture<OkexBxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OkexBxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OkexBxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
