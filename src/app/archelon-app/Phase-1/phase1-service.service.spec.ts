import { TestBed } from '@angular/core/testing';

import { Phase1ServiceService } from './phase1-service.service';

describe('Phase1ServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Phase1ServiceService = TestBed.get(Phase1ServiceService);
    expect(service).toBeTruthy();
  });
});
