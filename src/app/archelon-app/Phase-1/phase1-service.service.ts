import { Injectable ,OnInit } from '@angular/core';
import { HttpHeaders,  HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class Phase1ServiceService {

  constructor(private _http: HttpClient) { }


  getInitialPhase1(exchangeMoreId: number , exchangeLessId: number ):any {
    var headers = new HttpHeaders();
    headers.append("Content-type","application/json; charset=utf-8");
    console.log(exchangeMoreId);
    return this._http.post('http://149.28.154.112:4568/archelon/getInitialPhase1',{"exchangeMoreId" : exchangeMoreId , "exchangeLessId": exchangeLessId}, { headers :headers} );

  }

  saveInfoPhase1(dataPh1:any):any{
    var headers = new HttpHeaders();
    headers.append("Content-type","json/data; charset=utf-8");

    return this._http.post('http://149.28.154.112:4568/archelon/saveInfoPhase1',dataPh1, { headers :headers} );


  }


  

  testFunc():any{
    var headers = new HttpHeaders();
    headers.append("Content-type","application/json; charset=utf-8");

    return this._http.get('http://149.28.154.112:4568/archelon/test', { headers :headers} );


  }
       
}
