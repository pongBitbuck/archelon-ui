import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ph21BxBinanceBxComponent } from './ph21-bx-binance-bx.component';

describe('Ph21BxBinanceBxComponent', () => {
  let component: Ph21BxBinanceBxComponent;
  let fixture: ComponentFixture<Ph21BxBinanceBxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ph21BxBinanceBxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ph21BxBinanceBxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
