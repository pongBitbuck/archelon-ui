import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ph22BxBinanceBxComponent } from './ph22-bx-binance-bx.component';

describe('Ph22BxBinanceBxComponent', () => {
  let component: Ph22BxBinanceBxComponent;
  let fixture: ComponentFixture<Ph22BxBinanceBxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ph22BxBinanceBxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ph22BxBinanceBxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
