import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ph23BinanceBxBinanceComponent } from './ph23-binance-bx-binance.component';

describe('Ph23BinanceBxBinanceComponent', () => {
  let component: Ph23BinanceBxBinanceComponent;
  let fixture: ComponentFixture<Ph23BinanceBxBinanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ph23BinanceBxBinanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ph23BinanceBxBinanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
