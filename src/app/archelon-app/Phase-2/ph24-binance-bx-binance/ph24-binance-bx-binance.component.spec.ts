import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ph24BinanceBxBinanceComponent } from './ph24-binance-bx-binance.component';

describe('Ph24BinanceBxBinanceComponent', () => {
  let component: Ph24BinanceBxBinanceComponent;
  let fixture: ComponentFixture<Ph24BinanceBxBinanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ph24BinanceBxBinanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ph24BinanceBxBinanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
