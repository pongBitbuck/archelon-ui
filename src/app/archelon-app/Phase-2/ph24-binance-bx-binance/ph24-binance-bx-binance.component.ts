import { Component, OnInit } from '@angular/core';
import { Phase2ServiceService } from '../phase2-service.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-ph24-binance-bx-binance',
  templateUrl: './ph24-binance-bx-binance.component.html',
  styleUrls: ['./ph24-binance-bx-binance.component.scss'],
  providers: [Phase2ServiceService]
})
export class Ph24BinanceBxBinanceComponent implements OnInit {
  itemCoin : Array<Phase2Item> = new Array<Phase2Item>();

  constructor(private _phase2ServiceService : Phase2ServiceService ,  private router : Router) { }

  ngOnInit() {
    var that = this;
    this._phase2ServiceService.getInitialPhase2(4).subscribe(data => {
      data.result.forEach(element => {
        that.itemCoin.push({coin: element.shortName,valueCon1 :element.condition1Value,valueCon2 :element.condition2Value , id: element.id});
      });
    });
  }



  onCancleClick() {
    this.router.navigate(["refresh-page/" + this.router.url.replace(/\//g, '^')]);
  }

  onSave() {
    console.log(this.itemCoin);
    this._phase2ServiceService.saveInfoPhase2(this.itemCoin).subscribe(data => {
        setTimeout(() => this.onCancleClick(), 2500);

    });

  }

}

export interface Phase2Item{
  coin : string;
  valueCon1: number;
  valueCon2: number;
  id:number;

}