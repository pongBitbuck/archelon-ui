import { TestBed } from '@angular/core/testing';

import { Phase2ServiceService } from './phase2-service.service';

describe('Phase2ServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Phase2ServiceService = TestBed.get(Phase2ServiceService);
    expect(service).toBeTruthy();
  });
});
