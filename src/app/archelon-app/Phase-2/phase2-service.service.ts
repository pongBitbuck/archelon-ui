import { Injectable } from '@angular/core';
import { HttpHeaders,  HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class Phase2ServiceService {

  constructor(private _http: HttpClient) { }


  getInitialPhase2(subPhase: number ):any {
    var headers = new HttpHeaders();
    headers.append("Content-type","application/json; charset=utf-8");
    return this._http.post('http://149.28.154.112:4568/archelon/getInitialPhase2',{"subPhase" : subPhase }, { headers :headers} );

  }

  saveInfoPhase2(dataPh2:any):any{
    var headers = new HttpHeaders();
    headers.append("Content-type","json/data; charset=utf-8");

    return this._http.post('http://149.28.154.112:4568/archelon/saveInfoPhase2',dataPh2, { headers :headers} );
  }

}
