import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ph3BsvOkexBxComponent } from './ph3-bsv-okex-bx.component';

describe('Ph3BsvOkexBxComponent', () => {
  let component: Ph3BsvOkexBxComponent;
  let fixture: ComponentFixture<Ph3BsvOkexBxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ph3BsvOkexBxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ph3BsvOkexBxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
