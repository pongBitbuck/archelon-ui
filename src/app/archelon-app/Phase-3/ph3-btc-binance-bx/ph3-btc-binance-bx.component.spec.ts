import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ph3BtcBinanceBxComponent } from './ph3-btc-binance-bx.component';

describe('Ph3BtcBinanceBxComponent', () => {
  let component: Ph3BtcBinanceBxComponent;
  let fixture: ComponentFixture<Ph3BtcBinanceBxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ph3BtcBinanceBxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ph3BtcBinanceBxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
