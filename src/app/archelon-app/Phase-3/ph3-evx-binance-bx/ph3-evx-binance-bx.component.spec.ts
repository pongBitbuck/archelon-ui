import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ph3EvxBinanceBxComponent } from './ph3-evx-binance-bx.component';

describe('Ph3EvxBinanceBxComponent', () => {
  let component: Ph3EvxBinanceBxComponent;
  let fixture: ComponentFixture<Ph3EvxBinanceBxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ph3EvxBinanceBxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ph3EvxBinanceBxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
