import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ph3LtcBinanceBxComponent } from './ph3-ltc-binance-bx.component';

describe('Ph3LtcBinanceBxComponent', () => {
  let component: Ph3LtcBinanceBxComponent;
  let fixture: ComponentFixture<Ph3LtcBinanceBxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ph3LtcBinanceBxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ph3LtcBinanceBxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
