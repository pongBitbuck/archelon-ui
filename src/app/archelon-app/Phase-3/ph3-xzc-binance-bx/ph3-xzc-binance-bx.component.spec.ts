import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ph3XzcBinanceBxComponent } from './ph3-xzc-binance-bx.component';

describe('Ph3XzcBinanceBxComponent', () => {
  let component: Ph3XzcBinanceBxComponent;
  let fixture: ComponentFixture<Ph3XzcBinanceBxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ph3XzcBinanceBxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ph3XzcBinanceBxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
