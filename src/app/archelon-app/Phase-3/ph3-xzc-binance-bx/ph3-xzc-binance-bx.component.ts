import { Component, OnInit } from '@angular/core';
import { Phase3ServiceService } from '../phase3-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-ph3-xzc-binance-bx',
  templateUrl: './ph3-xzc-binance-bx.component.html',
  styleUrls: ['./ph3-xzc-binance-bx.component.scss'],
  providers: [Phase3ServiceService]
})
export class Ph3XzcBinanceBxComponent implements OnInit {
  itemCoin : Phase3Item = {
    coin : "coin",
    valueFirst : 0,
    valueSecond : 0,
    id:0,
  };

  constructor(private _phase3ServiceService : Phase3ServiceService ,  private router : Router) { 
    
  }

  ngOnInit() {
    this.itemCoin;
    var that = this;
    this._phase3ServiceService.getInitialPhase3(5,2,1).subscribe(data => {
      this.itemCoin.coin = data.result[0].shortName;
      this.itemCoin.valueFirst = data.result[0].firstMoreAmount;
      this.itemCoin.valueSecond = data.result[0].secondMoreAmount;
      this.itemCoin.id = data.result[0].id;
    });
  }

  onCancleClick() {
    this.router.navigate(["refresh-page/" + this.router.url.replace(/\//g, '^')]);
  }

  onSave() {
    console.log(this.itemCoin);
    this._phase3ServiceService.saveInfoPhase3(this.itemCoin).subscribe(data => {
        setTimeout(() => this.onCancleClick(), 2500);

    });

  }

}

export interface Phase3Item{
  coin : string;
  valueFirst : number;
  valueSecond : number;
  id:number;

}