import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ph3ZecBinanceBxComponent } from './ph3-zec-binance-bx.component';

describe('Ph3ZecBinanceBxComponent', () => {
  let component: Ph3ZecBinanceBxComponent;
  let fixture: ComponentFixture<Ph3ZecBinanceBxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ph3ZecBinanceBxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ph3ZecBinanceBxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
