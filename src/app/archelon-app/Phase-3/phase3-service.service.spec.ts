import { TestBed } from '@angular/core/testing';

import { Phase3ServiceService } from './phase3-service.service';

describe('Phase3ServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Phase3ServiceService = TestBed.get(Phase3ServiceService);
    expect(service).toBeTruthy();
  });
});
