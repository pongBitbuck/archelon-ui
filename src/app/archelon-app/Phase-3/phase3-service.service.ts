import { Injectable } from '@angular/core';
import { HttpHeaders,  HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class Phase3ServiceService {

  constructor(private _http: HttpClient) { }

  getInitialPhase3(coinId: number,exchangeFirstId: number,exchangeSecondId: number ):any {
    var headers = new HttpHeaders();
    headers.append("Content-type","application/json; charset=utf-8");
    return this._http.post('http://149.28.154.112:4568/archelon/getInitialPhase3',{"coinId" : coinId ,"exchangeFirstId":exchangeFirstId,"exchangeSecondId":exchangeSecondId  }, { headers :headers} );
  }

  saveInfoPhase3(dataPh3:any):any{
    var headers = new HttpHeaders();
    headers.append("Content-type","json/data; charset=utf-8");

    return this._http.post('http://149.28.154.112:4568/archelon/saveInfoPhase3',dataPh3, { headers :headers} );
  }

}
