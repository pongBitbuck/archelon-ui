import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArchelonBodyComponent } from './archelon-body/archelon-body.component';
import { UIModule } from './ui/ui.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NiComponentsModule } from './ni-components/ni-components.module';
import { RouterModule } from '@angular/router';
import { BxBinanceComponent } from './Phase-1/bx-binance/bx-binance.component';
import { BxOkexComponent } from './Phase-1/bx-okex/bx-okex.component';
import { BxBittrexComponent } from './Phase-1/bx-bittrex/bx-bittrex.component';
import { BinanceBxComponent } from './Phase-1/binance-bx/binance-bx.component';
import { OkexBxComponent } from './Phase-1/okex-bx/okex-bx.component';
import { BittrexBxComponent } from './Phase-1/bittrex-bx/bittrex-bx.component';
import { GrapCompareComponent } from './Graph/grapcompare/grapcompare.component';
import { HttpClientModule } from '@angular/common/http';

import { Ph21BxBinanceBxComponent } from './Phase-2/ph21-bx-binance-bx/ph21-bx-binance-bx.component';
import { Ph22BxBinanceBxComponent } from './Phase-2/ph22-bx-binance-bx/ph22-bx-binance-bx.component';
import { Ph23BinanceBxBinanceComponent } from './Phase-2/ph23-binance-bx-binance/ph23-binance-bx-binance.component';
import { Ph24BinanceBxBinanceComponent } from './Phase-2/ph24-binance-bx-binance/ph24-binance-bx-binance.component';
import { Ph3BtcBinanceBxComponent } from './Phase-3/ph3-btc-binance-bx/ph3-btc-binance-bx.component';
import { Ph3BsvOkexBxComponent } from './Phase-3/ph3-bsv-okex-bx/ph3-bsv-okex-bx.component';
import { Ph3EvxBinanceBxComponent } from './Phase-3/ph3-evx-binance-bx/ph3-evx-binance-bx.component';
import { Ph3LtcBinanceBxComponent } from './Phase-3/ph3-ltc-binance-bx/ph3-ltc-binance-bx.component';
import { Ph3XzcBinanceBxComponent } from './Phase-3/ph3-xzc-binance-bx/ph3-xzc-binance-bx.component';
import { Ph3ZecBinanceBxComponent } from './Phase-3/ph3-zec-binance-bx/ph3-zec-binance-bx.component';
import { RebalanceComponent } from './rebalance/rebalance.component';
import { UsdForCalculateComponent } from './usd-for-calculate/usd-for-calculate.component';
import { BtcForCalculateComponent } from './btc-for-calculate/btc-for-calculate.component';
import { RefreshPageComponent } from './refresh-page/refresh-page.component';

@NgModule({
  declarations: [ArchelonBodyComponent
    , BxBinanceComponent
    , BxOkexComponent
    , BxBittrexComponent
    , BinanceBxComponent
    , OkexBxComponent
    , BittrexBxComponent
    , GrapCompareComponent ,
    Ph21BxBinanceBxComponent,
    Ph22BxBinanceBxComponent,
    Ph23BinanceBxBinanceComponent,
    Ph24BinanceBxBinanceComponent,
    Ph3BtcBinanceBxComponent,
    Ph3BsvOkexBxComponent,
    Ph3EvxBinanceBxComponent,
    Ph3LtcBinanceBxComponent,
    Ph3XzcBinanceBxComponent,
    Ph3ZecBinanceBxComponent,
    RebalanceComponent,
    UsdForCalculateComponent,
    BtcForCalculateComponent,
    RefreshPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    UIModule,
    HttpClientModule,
    NiComponentsModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports : [
    ArchelonBodyComponent
  ]
})
export class ArchelonAppModule { }
