import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchelonBodyComponent } from './archelon-body.component';

describe('ArchelonBodyComponent', () => {
  let component: ArchelonBodyComponent;
  let fixture: ComponentFixture<ArchelonBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchelonBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchelonBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
