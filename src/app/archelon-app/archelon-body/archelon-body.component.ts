import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'archelon-app',
  templateUrl: 'archelon-body.component.html',
  styleUrls: ['../layouts.scss']
})
export class ArchelonBodyComponent implements OnInit {
  pageTitle: any;
  boxed: boolean;
  compress: boolean;
  menuStyle: string;
  rtl: boolean;
  @Input() openedSidebar: boolean;

  constructor( ) {
    this.openedSidebar = false;
    this.boxed = false;
    this.compress = false;
    this.menuStyle = 'style-3';
    this.rtl = false;

  }

  ngOnInit() { }

  getClasses() {
    let menu: string = (this.menuStyle);

    return {
      ['menu-' + menu]: menu,
      'boxed': this.boxed,
      'compress-vertical-navbar': this.compress,
      'open-sidebar': this.openedSidebar,
      'rtl': this.rtl
    };
  }

  sidebarState() {
    this.openedSidebar = !this.openedSidebar;
  }
}