import { TestBed } from '@angular/core/testing';

import { BtcForCalculateServiceService } from './btc-for-calculate-service.service';

describe('BtcForCalculateServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BtcForCalculateServiceService = TestBed.get(BtcForCalculateServiceService);
    expect(service).toBeTruthy();
  });
});
