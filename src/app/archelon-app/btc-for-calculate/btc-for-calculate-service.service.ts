import { Injectable } from '@angular/core';
import { HttpHeaders,  HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BtcForCalculateServiceService {
  constructor(private _http: HttpClient) { }


  getInitialBtcForCalculate():any {
    var headers = new HttpHeaders();
    headers.append("Content-type","application/json; charset=utf-8");
    return this._http.post('http://149.28.154.112:4568/archelon/getInitialBtcForCalculate', { headers :headers} );
  }

  saveInfoBtcForCalculate(btcForCal:any):any{
    var headers = new HttpHeaders();
    headers.append("Content-type","json/data; charset=utf-8");

    return this._http.post('http://149.28.154.112:4568/archelon/saveInfoBtcForCalculate',{value:btcForCal}, { headers :headers} );
  }

}
