import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtcForCalculateComponent } from './btc-for-calculate.component';

describe('BtcForCalculateComponent', () => {
  let component: BtcForCalculateComponent;
  let fixture: ComponentFixture<BtcForCalculateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtcForCalculateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtcForCalculateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
