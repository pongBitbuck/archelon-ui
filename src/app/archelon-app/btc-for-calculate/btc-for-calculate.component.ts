import { Component, OnInit } from '@angular/core';
import { BtcForCalculateServiceService } from './btc-for-calculate-service.service'
import {Router} from '@angular/router';

@Component({
  selector: 'app-btc-for-calculate',
  templateUrl: './btc-for-calculate.component.html',
  styleUrls: ['./btc-for-calculate.component.scss'],
  providers: [BtcForCalculateServiceService]
})
export class BtcForCalculateComponent implements OnInit {

  btcForCal : number;
  constructor(private btcForCalculateServiceService: BtcForCalculateServiceService ,  private router : Router ) { }

  ngOnInit() {
    var that = this;
    this.btcForCalculateServiceService.getInitialBtcForCalculate().subscribe(data => {
       
        this.btcForCal = data.result[0].valueDecimal;
        
    });
  }


  onCancleClick() {
    this.router.navigate(["refresh-page/" + this.router.url.replace(/\//g, '^')]);
  }

  onSave() {
    console.log(this.btcForCal);
    this.btcForCalculateServiceService.saveInfoBtcForCalculate(this.btcForCal).subscribe(data => {
        setTimeout(() => this.onCancleClick(), 2500);

    });

  }



}
