import { TestBed } from '@angular/core/testing';

import { RebalanceServiceService } from './rebalance-service.service';

describe('RebalanceServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RebalanceServiceService = TestBed.get(RebalanceServiceService);
    expect(service).toBeTruthy();
  });
});
