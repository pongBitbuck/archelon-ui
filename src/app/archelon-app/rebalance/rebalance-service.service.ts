import { Injectable } from '@angular/core';
import { HttpHeaders,  HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RebalanceServiceService {

  constructor(private _http: HttpClient) { }

  getInitialRebalance():any {
    var headers = new HttpHeaders();
    headers.append("Content-type","application/json; charset=utf-8");
    return this._http.post('http://149.28.154.112:4568/archelon/getInitialRebalance', { headers :headers} );
  }

  saveInfoRebalance(dataRebalance:any):any{
    var headers = new HttpHeaders();
    headers.append("Content-type","json/data; charset=utf-8");

    return this._http.post('http://149.28.154.112:4568/archelon/saveInfoRebalance',{ value : dataRebalance}, { headers :headers} );
  }

}
