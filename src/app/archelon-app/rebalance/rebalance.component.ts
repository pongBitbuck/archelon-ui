import { Component, OnInit } from '@angular/core';
import { RebalanceServiceService } from './rebalance-service.service'
import {Router} from '@angular/router';

const rebalanceList = [
];

@Component({
  selector: 'app-rebalance',
  templateUrl: './rebalance.component.html',
  styleUrls: ['./rebalance.component.scss']
})
export class RebalanceComponent implements OnInit {
  rebalanceList = rebalanceList;
  rebalanceValue : number;
  
  constructor(private rebalanceServiceService: RebalanceServiceService ,   private router : Router) {


   }

  ngOnInit() {
    var that = this;
    this.rebalanceServiceService.getInitialRebalance().subscribe(data => {
        data.rebalance.result.forEach(element => {
          this.rebalanceList.push( {value: element.id , viewValue: parseInt(element.amountStart) + ":" + parseInt(element.amountEnd) , valueSelect:parseInt(element.amountStart)  } );
        });
        this.rebalanceValue = data.valueConfig.result[0].valueDecimal;
        
    });
  }


  onCancleClick() {
    this.router.navigate(["refresh-page/" + this.router.url.replace(/\//g, '^')]);
  }

  onSave() {
    console.log(this.rebalanceValue);
    this.rebalanceServiceService.saveInfoRebalance(this.rebalanceValue).subscribe(data => {
        setTimeout(() => this.onCancleClick(), 2500);

    });

  }

}
