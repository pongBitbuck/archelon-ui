import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-refresh-page',
  templateUrl: './refresh-page.component.html',
  styleUrls: ['./refresh-page.component.scss']
})
export class RefreshPageComponent implements OnInit {

  constructor(private router: Router,
    // tslint:disable-next-line:variable-name
    private _Activatedroute: ActivatedRoute) { }

  ngOnInit() {
    this._Activatedroute.params.subscribe(params => {
      window.scrollTo(0, 0);
      // var page = params.page;
      // var Username = params.username;
      // var Password = params.password;
      const urlRoute = params.routePath.replace(/\^/g, '/');

      this.router.navigate([urlRoute]);

    });
  }

}
