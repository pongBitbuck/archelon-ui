import { throwError as observableThrowError } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const patnUrl = {
	pro:"/cryptobackend",
	dev:""
}
@Injectable({
	providedIn: 'root'
})
export class MenuService {
	constructor(private http: HttpClient) { }
	public getData() {
		const URL: string = 'assets/data/archelon-menu.json';
		return this.http.get(URL);
	}

	public handleError(error: any) {
		return observableThrowError(error.error || 'Server Error');
	}
}
