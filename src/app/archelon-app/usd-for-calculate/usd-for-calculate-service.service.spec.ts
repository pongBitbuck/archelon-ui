import { TestBed } from '@angular/core/testing';

import { UsdForCalculateServiceService } from './usd-for-calculate-service.service';

describe('UsdForCalculateServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsdForCalculateServiceService = TestBed.get(UsdForCalculateServiceService);
    expect(service).toBeTruthy();
  });
});
