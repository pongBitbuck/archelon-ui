import { Injectable } from '@angular/core';
import { HttpHeaders,  HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsdForCalculateServiceService {

  constructor(private _http: HttpClient) { }


  getInitialUsdForCalculate():any {
    var headers = new HttpHeaders();
    headers.append("Content-type","application/json; charset=utf-8");
    return this._http.post('http://149.28.154.112:4568/archelon/getInitialUsdForCalculate', { headers :headers} );
  }

  saveInfoUsdForCalculate(usdForCal:any):any{
    var headers = new HttpHeaders();
    headers.append("Content-type","json/data; charset=utf-8");

    return this._http.post('http://149.28.154.112:4568/archelon/saveInfoUsdForCalculate',{value:usdForCal}, { headers :headers} );
  }

  
}
