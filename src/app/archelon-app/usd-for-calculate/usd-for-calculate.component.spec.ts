import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsdForCalculateComponent } from './usd-for-calculate.component';

describe('UsdForCalculateComponent', () => {
  let component: UsdForCalculateComponent;
  let fixture: ComponentFixture<UsdForCalculateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsdForCalculateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsdForCalculateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
