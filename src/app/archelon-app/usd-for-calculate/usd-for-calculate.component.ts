import { Component, OnInit } from '@angular/core';
import { UsdForCalculateServiceService } from './usd-for-calculate-service.service'
import {Router} from '@angular/router';

@Component({
  selector: 'app-usd-for-calculate',
  templateUrl: './usd-for-calculate.component.html',
  styleUrls: ['./usd-for-calculate.component.scss']
})
export class UsdForCalculateComponent implements OnInit {

  usdForCal : number;
  constructor(private usdForCalculateServiceService: UsdForCalculateServiceService ,   private router : Router) { }

  ngOnInit() {
    var that = this;
    this.usdForCalculateServiceService.getInitialUsdForCalculate().subscribe(data => {
       
        this.usdForCal = data.result[0].valueDecimal;
        
    });
  }

  onCancleClick() {
    this.router.navigate(["refresh-page/" + this.router.url.replace(/\//g, '^')]);
  }

  onSave() {
    console.log(this.usdForCal);
    this.usdForCalculateServiceService.saveInfoUsdForCalculate(this.usdForCal).subscribe(data => {
        setTimeout(() => this.onCancleClick(), 2500);

    });

  }

}
