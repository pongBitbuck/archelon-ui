import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArbitrageconfigComponent } from './arbitrageconfig.component';

describe('ArbitrageconfigComponent', () => {
  let component: ArbitrageconfigComponent;
  let fixture: ComponentFixture<ArbitrageconfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArbitrageconfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArbitrageconfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
